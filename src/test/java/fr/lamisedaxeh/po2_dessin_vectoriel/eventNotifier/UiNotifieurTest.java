package fr.lamisedaxeh.po2_dessin_vectoriel.eventNotifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.lamisedaxeh.po2_dessin_vectoriel.event.UiEvent;
import fr.lamisedaxeh.po2_dessin_vectoriel.eventListener.UiListener;

public class UiNotifieurTest implements UiListener {
	private Boolean eventListenerOK = false;
	private UiEvent monUiEvent;

	private final UiNotifieur monUiNotifieur = new UiNotifieur();

	public void actionADeclancher(final UiEvent evt) {
		eventListenerOK = true;
	}

	@BeforeEach
	public void setUp() throws Exception {
		monUiEvent = new UiEvent(this, "mesSuperDonnee");
		monUiNotifieur.addUiEventListener(this);
	}

	@Test
	public final void testDiffuserFromeEvent() throws Exception {
		monUiNotifieur.diffuserFromeEvent(monUiEvent);
		Thread.sleep(100);
		assertTrue(eventListenerOK);
	}

	@Test
			public final void testRemoveUiEventListener() throws Exception {
				monUiNotifieur.removeUiEventListener(this);
				monUiNotifieur.diffuserFromeEvent(monUiEvent);
				Thread.sleep(100);
				assertFalse(eventListenerOK);
		
			}

}
