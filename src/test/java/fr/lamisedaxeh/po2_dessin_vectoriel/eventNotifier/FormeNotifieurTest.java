package fr.lamisedaxeh.po2_dessin_vectoriel.eventNotifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.lamisedaxeh.po2_dessin_vectoriel.event.FormeEvent;
import fr.lamisedaxeh.po2_dessin_vectoriel.eventListener.FormeListener;

public class FormeNotifieurTest implements FormeListener {
	private Boolean eventListenerOK = false;
	private FormeEvent monFormeEvent;

	private final FormeNotifieur monFormeNotifieur = new FormeNotifieur();

	public void actionADeclancher(final FormeEvent evt) {
		eventListenerOK = true;
	}

	@BeforeEach
	public void setUp() throws Exception {
		monFormeEvent = new FormeEvent(this, "mesSuperDonnee");
		monFormeNotifieur.addFormeEventListener(this);
	}

	@Test
	public final void testDiffuserFromeEvent() throws Exception {
		monFormeNotifieur.diffuserFromeEvent(monFormeEvent);
		Thread.sleep(100);
		assertTrue(eventListenerOK);
	}

	@Test
	public final void testRemoveFormeEventListener() throws Exception {
		monFormeNotifieur.removeFormeEventListener(this);
		monFormeNotifieur.diffuserFromeEvent(monFormeEvent);
		Thread.sleep(100);
		assertFalse(eventListenerOK);

	}
}
