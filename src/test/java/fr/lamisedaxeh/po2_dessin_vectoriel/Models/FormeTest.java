/**
 *
 */
package fr.lamisedaxeh.po2_dessin_vectoriel.Models;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author lamisedaxeh
 *
 */
public class FormeTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
		// System.out.println("Executing a " + this.toString() + " test file");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	public static void tearDownAfterClass() throws Exception {
		//System.out.println("Execution of " + this.toString() + " test file done");
	}

	private Forme maForme;

	private final String nomForme = "testForme";

	private ArrayList<Point> listePoints;

	private final Color maCouleur = new Color(15, 57, 96);

	/**
	 * Pour chaque debuts de test
	 *
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	public void setUp() throws Exception {
		System.out.println("Executing a new test");
		// ---

		listePoints = new ArrayList<Point>();
		listePoints.add(new Point(0, 0));
		listePoints.add(new Point(1, 1));
		listePoints.add(new Point(2, 2));
		listePoints.add(new Point());

		maForme = new Forme(nomForme, maCouleur, listePoints);
	}

	/**
	 * Pour chaque fin de test
	 * @throws java.lang.Exception
	 */
	@AfterEach
	public void tearDown() throws Exception {
		System.out.println("Execution done");
	}

	@Test
	public void testApp() throws Exception {
		assertTrue(true);
	}

	/**
	 * Test method for {@link fr.lamisedaxeh.po2_dessin_vectoriel.Models.Forme#Forme(java.lang.String, java.awt.Color, java.util.ArrayList)}.
	 */
	@Test
	public void testForme() throws Exception {
		assertEquals(nomForme, maForme.getNom());
		assertEquals(maCouleur, maForme.getCouleur());
		assertEquals(listePoints, maForme.getListPoints());
	}

	/**
	 * Test method for {@link fr.lamisedaxeh.po2_dessin_vectoriel.Models.Forme#getCouleur()}.
	 */
	@Test
	public void testGetCouleur() throws Exception {
		assertEquals(maCouleur, maForme.getCouleur());
	}

	/**
	 * Test method for {@link fr.lamisedaxeh.po2_dessin_vectoriel.Models.Forme#getListPoints()}.
	 */
	@Test
	public void testGetListPoints() throws Exception {
		assertEquals(listePoints, maForme.getListPoints());
	}

	/**
	 * Test method for {@link fr.lamisedaxeh.po2_dessin_vectoriel.Models.Forme#setCouleur(java.awt.Color)}.
	 */
	@Test
	public void testSetCouleur() throws Exception {

		assertEquals(maCouleur, maForme.getCouleur());
		final Color newColor = new Color(127, 42, 23);
		maForme.setCouleur(newColor);
		assertEquals(newColor, maForme.getCouleur());

	}

	/**
	 * Test method for {@link fr.lamisedaxeh.po2_dessin_vectoriel.Models.Forme#setListPoints(java.util.ArrayList)}.
	 */
	@Test
	public void testSetListPoints() throws Exception {
		assertEquals(listePoints, maForme.getListPoints());
		final ArrayList<Point> newListePoints = new ArrayList<Point>();
		maForme.setListPoints(newListePoints);
		assertEquals(newListePoints, maForme.getListPoints());
	}

	/**
	 * Test method for {@link fr.lamisedaxeh.po2_dessin_vectoriel.Models.Forme#toString()}.
	 */
	@Test
	public void testToString() throws Exception {
		assertNotNull(maForme.toString());
	}

}
