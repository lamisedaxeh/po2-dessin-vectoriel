package fr.lamisedaxeh.po2_dessin_vectoriel.event;

import java.util.EventObject;

/**
 * This object contains the info of Event : source and data
 *
 * @author lamisedaxeh
 */
public class UiEvent extends EventObject {
	private static final long serialVersionUID = 5213944089806425841L;
	/**
	 * Data of the Event
	 */
	private final Object donnee;

	/**
	 * Setup Form event
	 *
	 * @param source source of the event
	 * @param donnee Data to transfer
	 */
	public UiEvent(final Object source, final Object donnee) {
		super(source);
		this.donnee = donnee;
	}

	/**
	 * Get data send by notifier
	 *
	 * @return The data
	 */
	public Object getDonnee() {
		return donnee;
	}

}
