package fr.lamisedaxeh.po2_dessin_vectoriel.eventListener;

import fr.lamisedaxeh.po2_dessin_vectoriel.event.FormeEvent;

/**
 * An interface to be implemented by everyone, who interested in "Forme" events
 *
 * @author lamisedaxeh
 */
public interface FormeListener extends java.util.EventListener {
	/**
	 * Someone send 'MSG'
	 *
	 * @param evt FormeEvent with data of the event
	 */
	public void actionADeclancher(FormeEvent evt);
}
