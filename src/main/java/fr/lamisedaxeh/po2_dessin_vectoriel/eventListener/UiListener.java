package fr.lamisedaxeh.po2_dessin_vectoriel.eventListener;

import fr.lamisedaxeh.po2_dessin_vectoriel.event.UiEvent;

/**
 * An interface to be implemented by everyone, who interested in "UI" events
 *
 * @author lamisedaxeh
 */
public interface UiListener extends java.util.EventListener {
	/**
	 * Someone send 'MSG'
	 *
	 * @param evt FormeEvent with data of the event
	 */
	public void actionADeclancher(UiEvent evt);
}
