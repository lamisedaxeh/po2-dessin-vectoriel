package fr.lamisedaxeh.po2_dessin_vectoriel.Controllers;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lamisedaxeh.po2_dessin_vectoriel.Models.Forme;
import fr.lamisedaxeh.po2_dessin_vectoriel.Models.ListeForme;
import fr.lamisedaxeh.po2_dessin_vectoriel.event.FormeEvent;
import fr.lamisedaxeh.po2_dessin_vectoriel.event.UiEvent;
import fr.lamisedaxeh.po2_dessin_vectoriel.eventListener.UiListener;
import fr.lamisedaxeh.po2_dessin_vectoriel.eventNotifier.FormeNotifieur;

/**
 * This class manage all the forme
 *
 * @author lamisedaxeh
 */
public class ListeFormeController implements UiListener {
	private static final Logger logger = LoggerFactory.getLogger(ListeFormeController.class);

	/** Notifier for the event of the Forme like update */
	private final FormeNotifieur formeNotifieur;

	private final ListeForme listeForme = new ListeForme();

	private final Forme selectedForme;

	// setup ->
	public ListeFormeController(final FormeNotifieur formeNotifieur) {
		this.formeNotifieur = formeNotifieur;
		final ArrayList<Point> listPoint = new ArrayList<Point>();
		selectedForme = new Forme("Default", new Color(1, 1, 1), listPoint);
		final FormeEvent fromeEvt = new FormeEvent(this, selectedForme);
		formeNotifieur.diffuserFromeEvent(null);
	}

	/**
	 * Catch UI change
	 */
	public void actionADeclancher(final UiEvent evt) {
		boolean flag = false;
		if (evt.getDonnee() instanceof Color) {
			flag = true;
			selectedForme.setCouleur((Color) evt.getDonnee());
			logger.info(
					"La couleur de " + selectedForme.getNom() + "à était changé par : "
							+ selectedForme.getCouleur().toString());
		} else if (evt.getDonnee() instanceof Point) {
			flag = true;
			selectedForme.addPoint((Point) evt.getDonnee());
			logger.info("Le point " + evt.getDonnee().toString() + "à était ajouté à " + selectedForme.getNom());
		}
		else if (evt.getDonnee() instanceof String) {
			flag = true;
			logger.info(
					"Le nom de " + selectedForme.getNom() + "à était changé par : " + (String) evt.getDonnee());
			selectedForme.setNom((String) evt.getDonnee());
		}
	}
}
