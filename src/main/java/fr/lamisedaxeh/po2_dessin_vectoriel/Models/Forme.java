package fr.lamisedaxeh.po2_dessin_vectoriel.Models;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

/**
 * Forme pour
 *
 * @author lamisedaxeh
 *
 */
public class Forme {

	private String nom;
	private Color couleur;

	private ArrayList<Point> listPoints;

	public Forme(final String nom, final Color couleur, final ArrayList<Point> listPoints) {
		super();
		this.nom = nom;
		this.couleur = couleur;
		this.listPoints = listPoints;
	}

	/**
	 * Add the new point to {@link #listPoints}.
	 *
	 * @param p the point to add
	 */
	public void addPoint(final Point p) {
		listPoints.add(p);
	}

	/**
	 * @return the couleur
	 */
	public Color getCouleur() {
		return couleur;
	}

	/**
	 * @return the listPoints
	 */
	public ArrayList<Point> getListPoints() {
		return listPoints;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param couleur the couleur to set
	 */
	public void setCouleur(final Color couleur) {
		this.couleur = couleur;
	}

	/**
	 * @param listPoints the listPoints to set
	 */
	public void setListPoints(final ArrayList<Point> listPoints) {
		this.listPoints = listPoints;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(final String nom) {
		this.nom = nom;
	}




	@Override
	public String toString() {
		return "Forme [nom=" + nom + ", couleur=" + couleur + ", listPoints=" + listPoints + "]";
	}

}
