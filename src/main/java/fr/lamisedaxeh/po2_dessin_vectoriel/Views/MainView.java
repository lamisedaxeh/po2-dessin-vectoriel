package fr.lamisedaxeh.po2_dessin_vectoriel.Views;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import fr.lamisedaxeh.po2_dessin_vectoriel.eventNotifier.UiNotifieur;

public class MainView extends JPanel {
	private static final long serialVersionUID = -5523748436928459328L;

	public MainView(final UiNotifieur uiNotifieur) {
		this.setLayout(new GridBagLayout());

		final GridBagConstraints c = new GridBagConstraints();

		c.insets = new Insets(5, 5, 5, 5);

		c.weightx = 1.0;
		c.weighty = 0.10;

		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 0;
		this.add(new ToolBox(uiNotifieur), c);

		c.weighty = 1.0;
		c.gridwidth = 1;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 1;
		this.add(new GraphPanelView(), c);
		c.weightx = 0.10;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 1;
		this.add(new ListFormeInfoView(), c);
		repaint();

	}
}
