package fr.lamisedaxeh.po2_dessin_vectoriel.Views;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.lamisedaxeh.po2_dessin_vectoriel.eventNotifier.FormeNotifieur;

public class ToolsView extends JPanel{
	private static final long serialVersionUID = 1239623787947093641L;

	private final FormeNotifieur monFormeNotifieur = new FormeNotifieur();

	private final JLabel xLabel = new JLabel("pos X : ");
	private final JLabel yLabel = new JLabel("pos Y :");
	private final JLabel nomLabel = new JLabel("nom :");

	private final JTextField xTextField = new JTextField();
	private final JTextField yTextField = new JTextField();
	private final JTextField nomTextField = new JTextField();

	// Send event went it's push
	private final JButton newForme = new JButton("New Forme");

	private final JButton remForme = new JButton("Rem Forme");

	public ToolsView() {
		super();
		setLayout(new GridBagLayout());

		final GridBagConstraints c = new GridBagConstraints();

		c.insets = new Insets(5, 5, 5, 5);

		c.fill = GridBagConstraints.BOTH;
		c.weightx = 0.25;
		c.weighty = 1;
		c.gridy = 0;
		c.gridx = 0;
		this.add(xLabel, c);
		c.weightx = 0.75;
		c.gridx = 1;
		this.add(xTextField, c);
		c.weightx = 0.25;
		c.gridy = 1;
		c.gridx = 0;
		this.add(yLabel, c);
		c.weightx = 0.75;
		c.gridx = 1;
		this.add(yTextField, c);
		c.weightx = 0.25;
		c.gridy = 2;
		c.gridx = 0;
		this.add(nomLabel, c);
		c.weightx = 0.75;
		c.gridx = 1;
		this.add(nomTextField, c);

		c.weightx = 0.25;
		c.gridy = 3;
		c.gridx = 0;
		this.add(newForme, c);

		c.weightx = 0.25;
		c.gridy = 3;
		c.gridx = 1;
		this.add(remForme, c);
	}
}
