package fr.lamisedaxeh.po2_dessin_vectoriel.Views;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import fr.lamisedaxeh.po2_dessin_vectoriel.eventNotifier.UiNotifieur;

public class ToolBox extends DefaultBloc{
	private static final long serialVersionUID = -1651438189018471500L;

	private final ToolsView toolsView;
	private final ColorBoxView colorBoxView;

	public ToolBox(final UiNotifieur uiNotifieur) {
		super("Tools Box");
		toolsView = new ToolsView();
		colorBoxView = new ColorBoxView(uiNotifieur);

		this.setLayout(new GridBagLayout());
		final GridBagConstraints c = new GridBagConstraints();



		c.weightx = 1.0;
		c.weighty = 1.0;
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 0;
		this.add(toolsView, c);

		c.weightx = 0.25;
		c.gridwidth = 1;
		c.gridx = 2;
		c.gridy = 0;
		this.add(colorBoxView, c);

	}
}
