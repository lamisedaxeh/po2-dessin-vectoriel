package fr.lamisedaxeh.po2_dessin_vectoriel.Views;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.lamisedaxeh.po2_dessin_vectoriel.Models.Forme;

/**
 * A panel with all the data of a Forme
 *
 * @author lamisedaxeh
 */
public class FormeInfoView extends JPanel {
	private static final long serialVersionUID = -8092882337033977363L;

	private final JLabel infoLabel = new JLabel();

	/**
	 * Create new view Forme Info Disposition -> ""MaForme : {1,4...} ""
	 * 
	 * @param f Forme to get info
	 */
	public FormeInfoView(final Forme f) {
		infoLabel.setText(f.getNom() + " : " + f.getListPoints().toString());
		infoLabel.setForeground(f.getCouleur());

		this.add(infoLabel);
	}


}
