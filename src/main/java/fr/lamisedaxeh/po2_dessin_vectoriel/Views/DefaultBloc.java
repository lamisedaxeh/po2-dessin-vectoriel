package fr.lamisedaxeh.po2_dessin_vectoriel.Views;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class DefaultBloc extends JPanel{
	private static final long serialVersionUID = -771776331560164924L;

	public DefaultBloc(final String title) {
		super();
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), title));

		this.setLayout(new GridLayout());
	}
}
