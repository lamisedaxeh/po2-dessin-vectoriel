package fr.lamisedaxeh.po2_dessin_vectoriel.Views;

import java.awt.Graphics;

import javax.swing.JPanel;

public class GraphView extends JPanel {
	private static final long serialVersionUID = 6651760256515752046L;

	public GraphView() {
		super();
	}

	@Override
	public void paint(final Graphics g) {
		g.drawOval(10, 20, 50, 50);
		g.drawRect(30, 40, 80, 110);
		g.drawString("ceci est un long text", 10, 100);
	}

}
