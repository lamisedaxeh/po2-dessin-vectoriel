package fr.lamisedaxeh.po2_dessin_vectoriel.Views;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.lamisedaxeh.po2_dessin_vectoriel.event.UiEvent;
import fr.lamisedaxeh.po2_dessin_vectoriel.eventNotifier.UiNotifieur;

public class ColorBoxView extends DefaultBloc {
	private static final long serialVersionUID = 6430576122312815410L;

	private UiNotifieur uiNotifieur = new UiNotifieur();

	private final JLabel redLabel = new JLabel("R");
	private final JLabel greenLabel = new JLabel("G");
	private final JLabel blueLabel = new JLabel("B");

	// TODO set verif on text field is number lower or = than 255
	private final JTextField redTextField = new JTextField();
	private final JTextField greenTextField = new JTextField();
	private final JTextField blueTextField = new JTextField();

	/**
	 * Wait for color are update and send notification
	 */
	private final DocumentListener docListener = new DocumentListener() {
		public void changedUpdate(final DocumentEvent e) {
			System.out.println("changedUpdate");

			sendEvent();

		}

		public void insertUpdate(final DocumentEvent e) {

			System.out.println("insertUpdate");
			sendEvent();
		}

		public void removeUpdate(final DocumentEvent e) {

			System.out.println("removeUpdate");
			sendEvent();
		}

		public void sendEvent() {
			final UiEvent uiEvent = new UiEvent(this, new Color(Integer.parseInt(redTextField.getText()),
					Integer.parseInt(greenTextField.getText()), Integer.parseInt(blueTextField.getText())));
			uiNotifieur.diffuserFromeEvent(uiEvent);
		}
	};

	public ColorBoxView(final UiNotifieur uiNotifieur) {
		super("Couleur");
		this.uiNotifieur = uiNotifieur;
		redTextField.setText("0");
		greenTextField.setText("0");
		blueTextField.setText("0");

		setLayout(new GridBagLayout());

		final GridBagConstraints c = new GridBagConstraints();

		c.insets = new Insets(5, 5, 5, 5);

		c.fill = GridBagConstraints.BOTH;
		c.weightx = 0.25;
		c.weighty = 1;
		c.gridy = 0;
		c.gridx = 0;
		this.add(redLabel, c);
		c.weightx = 0.75;
		c.gridx = 1;
		this.add(redTextField, c);
		c.weightx = 0.25;
		c.gridy = 1;
		c.gridx = 0;
		this.add(greenLabel, c);
		c.weightx = 0.75;
		c.gridx = 1;
		this.add(greenTextField, c);
		c.weightx = 0.25;
		c.gridy = 2;
		c.gridx = 0;
		this.add(blueLabel, c);
		c.weightx = 0.75;
		c.gridx = 1;
		this.add(blueTextField, c);

		redTextField.getDocument().addDocumentListener(docListener);
		greenTextField.getDocument().addDocumentListener(docListener);
		blueTextField.getDocument().addDocumentListener(docListener);
	}


}
