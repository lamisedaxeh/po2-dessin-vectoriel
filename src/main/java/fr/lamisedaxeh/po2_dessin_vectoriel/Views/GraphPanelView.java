package fr.lamisedaxeh.po2_dessin_vectoriel.Views;

import java.awt.GridLayout;

public class GraphPanelView extends DefaultBloc {
	private static final long serialVersionUID = -2167249012186929184L;

	public GraphPanelView() {
		super("Vue graphique");
		this.setLayout(new GridLayout());
		this.add(new GraphView());


	}


}
