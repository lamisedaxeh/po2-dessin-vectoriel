package fr.lamisedaxeh.po2_dessin_vectoriel.Views;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;
import java.util.ArrayList;

import fr.lamisedaxeh.po2_dessin_vectoriel.Models.Forme;

/**
 * View left panel of the windows with info of all the Forme
 * @author lamisedaxeh
 */
public class ListFormeInfoView extends DefaultBloc {
	private static final long serialVersionUID = -745360474670658918L;



	public ListFormeInfoView() {
		super("Vue Objet");
		this.setLayout(new GridLayout(3, 1));
		this.add(new FormeInfoView(new Forme("Nom", new Color(8, 8, 255), new ArrayList<Point>())));
		this.add(new FormeInfoView(new Forme("Nom2", new Color(255, 3, 3), new ArrayList<Point>())));
		this.add(new FormeInfoView(new Forme("Nom3", new Color(2, 255, 2), new ArrayList<Point>())));
	}
}
