package fr.lamisedaxeh.po2_dessin_vectoriel;

import javax.swing.JFrame;

import fr.lamisedaxeh.po2_dessin_vectoriel.Controllers.ListeFormeController;
import fr.lamisedaxeh.po2_dessin_vectoriel.Views.MainView;
import fr.lamisedaxeh.po2_dessin_vectoriel.eventNotifier.FormeNotifieur;
import fr.lamisedaxeh.po2_dessin_vectoriel.eventNotifier.UiNotifieur;

/*
 *  I'd suggest you to call your controller SettingsController and model just Settings.
 *  It is because model actually contains you data. What kind of data does your model contain?
 *  The answer is: settings. So, call it settings.
 *
 *  Controller is different story.
 *  It is a class that deals with you data.
 *  There are probably many classes that deal with settings: SettingsBuilder, SettingsFactory, SettingsUtil, SettingsService etc.
 *  This one is controller, so call it SettingsController.
 */
/**
 * Main class of project with setup of main Jframe
 * @author lamisedaxeh
 */
public class App extends JFrame
{

	/** Notifier for the event of the UI like change in textBox */
	private final static UiNotifieur uiNotifieur = new UiNotifieur();
	/** Notifier for the event of the Forme like update */
	private final static FormeNotifieur formeNotifieur = new FormeNotifieur();

	/**
	 * Setup the app
	 *
	 * @param args
	 */
	public static void main(final String[] args) {
		final App app = new App();

		app.add(new MainView(uiNotifieur));
		final ListeFormeController listeFormeController = new ListeFormeController(formeNotifieur);
		uiNotifieur.addUiEventListener(listeFormeController);
		app.setVisible(true);

	}

	/**
	 * Setup the windows
	 */
	public App(){
		setSize(720, 400);
		setTitle("Ink-Scope");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
