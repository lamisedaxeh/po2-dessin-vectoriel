package fr.lamisedaxeh.po2_dessin_vectoriel.eventNotifier;

import javax.swing.event.EventListenerList;

import fr.lamisedaxeh.po2_dessin_vectoriel.event.FormeEvent;
import fr.lamisedaxeh.po2_dessin_vectoriel.eventListener.FormeListener;

/**
 * This class control the listening list and the transmission of the event
 *
 * @author lamisedaxeh
 */
public class FormeNotifieur {
	/**
	 * List of all the FormeLister listen the event
	 */
	private final EventListenerList listenerList = new EventListenerList();

	/**
	 * Add new FormeListener to the list {@link #listenerList} who listen the event
	 *
	 * @param listener The listener to add to the list
	 */
	public void addFormeEventListener(final FormeListener listener) {
		listenerList.add(FormeListener.class, listener);
	}

	/**
	 * Send the event at all listeners in {@link #listenerList} who are
	 * FormeListener
	 *
	 * @param evt Event to send at all listeners
	 */
	public void diffuserFromeEvent(final FormeEvent evt) {
		final Object[] listeners = listenerList.getListenerList();
		// +2 because getListenerList() return pair for perf
		for (int i = 0; i < listeners.length; i = i + 2) {
			// Send event at all FormeListener
			if (listeners[i] == FormeListener.class) {
				((FormeListener) listeners[i + 1]).actionADeclancher(evt);
			}
		}
	}

	/**
	 * Remove all listener of specific type
	 *
	 * @param type we are removed
	 */
	public void removeFormeEventListener(final FormeListener listener) {
		listenerList.remove(FormeListener.class, listener);
	}

}
