package fr.lamisedaxeh.po2_dessin_vectoriel.eventNotifier;

import javax.swing.event.EventListenerList;

import fr.lamisedaxeh.po2_dessin_vectoriel.event.UiEvent;
import fr.lamisedaxeh.po2_dessin_vectoriel.eventListener.UiListener;

/**
 * This class control the listening list and the transmission of the event
 *
 * @author lamisedaxeh
 */
public class UiNotifieur {
	/**
	 * List of all the UiLister listen the event
	 */
	private final EventListenerList listenerList = new EventListenerList();

	/**
	 * Add new UiListener to the list {@link #listenerList} who listen the event
	 *
	 * @param listener The listener to add to the list
	 */
	public void addUiEventListener(final UiListener listener) {
		listenerList.add(UiListener.class, listener);
	}

	/**
	 * Send the event at all listeners in {@link #listenerList} who are UiListener
	 *
	 * @param evt Event to send at all listeners
	 */
	public void diffuserFromeEvent(final UiEvent evt) {
		final Object[] listeners = listenerList.getListenerList();
		// +2 because getListenerList() return pair for perf
		for (int i = 0; i < listeners.length; i = i + 2) {
			// Send event at all UiListener
			if (listeners[i] == UiListener.class) {
				((UiListener) listeners[i + 1]).actionADeclancher(evt);
			}
		}
	}

	/**
	 * Remove all listener of specific type
	 *
	 * @param type we are removed
	 */
	public void removeUiEventListener(final UiListener listener) {
		listenerList.remove(UiListener.class, listener);
	}

}
